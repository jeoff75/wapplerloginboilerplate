<?php
require('../../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "settings": {
    "options": {}
  },
  "meta": {
    "options": {},
    "$_GET": [
      {
        "type": "text",
        "name": "sort"
      },
      {
        "type": "text",
        "name": "dir"
      },
      {
        "type": "text",
        "name": "language"
      }
    ]
  },
  "exec": {
    "steps": [
      {
        "name": "",
        "options": {
          "comment": "Get translation for given text and lang"
        }
      },
      "Connections/my_mysql",
      {
        "name": "getTranslation",
        "module": "dbconnector",
        "action": "select",
        "options": {
          "connection": "my_mysql",
          "sql": {
            "type": "SELECT",
            "columns": [
              {
                "table": "translations",
                "column": "text_key"
              },
              {
                "table": "translations",
                "column": "translation"
              }
            ],
            "table": {
              "name": "translations"
            },
            "joins": [],
            "wheres": {
              "condition": "AND",
              "rules": [
                {
                  "id": "translations.lang_key",
                  "field": "translations.lang_key",
                  "type": "string",
                  "operator": "equal",
                  "value": "{{$_GET.language}}",
                  "data": {
                    "table": "translations",
                    "column": "lang_key",
                    "type": "text"
                  },
                  "operation": "="
                }
              ],
              "conditional": null,
              "valid": true
            },
            "query": "SELECT text_key, translation\nFROM translations\nWHERE lang_key = :P1 /* {{$_GET.language}} */",
            "params": [
              {
                "operator": "equal",
                "type": "expression",
                "name": ":P1",
                "value": "{{$_GET.language}}"
              }
            ]
          }
        },
        "output": true,
        "meta": [
          {
            "name": "text_key",
            "type": "text"
          },
          {
            "name": "translation",
            "type": "text"
          }
        ],
        "outputType": "array"
      },
      {
        "name": "",
        "module": "core",
        "action": "condition",
        "options": {
          "if": "{{getTranslation.length}}",
          "then": {
            "steps": {
              "name": "",
              "options": {
                "comment": "Return current translations"
              }
            }
          },
          "else": {
            "steps": [
              {
                "name": "",
                "options": {
                  "comment": "Default to english if empty"
                }
              },
              {
                "name": "getTranslation",
                "module": "dbconnector",
                "action": "select",
                "options": {
                  "connection": "my_mysql",
                  "sql": {
                    "type": "SELECT",
                    "columns": [
                      {
                        "table": "translations",
                        "column": "text_key"
                      },
                      {
                        "table": "translations",
                        "column": "translation"
                      }
                    ],
                    "table": {
                      "name": "translations"
                    },
                    "joins": [],
                    "wheres": {
                      "condition": "AND",
                      "rules": [
                        {
                          "id": "translations.lang_key",
                          "field": "translations.lang_key",
                          "type": "string",
                          "operator": "equal",
                          "value": "en",
                          "data": {
                            "table": "translations",
                            "column": "lang_key",
                            "type": "text"
                          },
                          "operation": "="
                        }
                      ],
                      "conditional": null,
                      "valid": true
                    },
                    "query": "SELECT text_key, translation\nFROM translations\nWHERE lang_key = 'en'",
                    "params": []
                  }
                },
                "output": true,
                "meta": [
                  {
                    "name": "text_key",
                    "type": "text"
                  },
                  {
                    "name": "translation",
                    "type": "text"
                  }
                ],
                "outputType": "array"
              }
            ]
          }
        }
      }
    ]
  }
}
JSON
);
?>