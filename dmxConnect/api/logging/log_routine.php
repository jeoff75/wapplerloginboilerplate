<?php
require('../../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "settings": {
    "options": {}
  },
  "meta": {
    "options": {},
    "$_POST": [
      {
        "type": "datetime",
        "name": "log_date"
      },
      {
        "type": "text",
        "name": "log_type"
      },
      {
        "type": "text",
        "name": "log_message"
      }
    ]
  },
  "exec": {
    "steps": [
      "Connections/my_mysql",
      {
        "name": "insertLog",
        "module": "dbupdater",
        "action": "insert",
        "options": {
          "connection": "my_mysql",
          "sql": {
            "type": "insert",
            "values": [
              {
                "table": "logging",
                "column": "log_date",
                "type": "datetime",
                "value": "{{$_POST.log_date}}"
              },
              {
                "table": "logging",
                "column": "log_type",
                "type": "text",
                "value": "{{$_POST.log_type}}"
              },
              {
                "table": "logging",
                "column": "log_message",
                "type": "text",
                "value": "{{$_POST.log_message}}"
              }
            ],
            "table": "logging",
            "query": "INSERT INTO logging\n(log_date, log_type, log_message) VALUES (:P1 /* {{$_POST.log_date}} */, :P2 /* {{$_POST.log_type}} */, :P3 /* {{$_POST.log_message}} */)",
            "params": [
              {
                "name": ":P1",
                "type": "expression",
                "value": "{{$_POST.log_date}}"
              },
              {
                "name": ":P2",
                "type": "expression",
                "value": "{{$_POST.log_type}}"
              },
              {
                "name": ":P3",
                "type": "expression",
                "value": "{{$_POST.log_message}}"
              }
            ]
          }
        },
        "meta": [
          {
            "name": "identity",
            "type": "text"
          },
          {
            "name": "affected",
            "type": "number"
          }
        ]
      }
    ]
  }
}
JSON
);
?>