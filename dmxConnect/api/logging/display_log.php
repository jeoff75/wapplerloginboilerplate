<?php
require('../../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "settings": {
    "options": {}
  },
  "meta": {
    "options": {},
    "$_GET": [
      {
        "type": "text",
        "name": "sort"
      },
      {
        "type": "text",
        "name": "dir"
      },
      {
        "type": "text",
        "name": "offset"
      },
      {
        "type": "text",
        "name": "limit"
      }
    ],
    "$_POST": [
      {
        "type": "datetime",
        "name": "log_date"
      },
      {
        "type": "text",
        "name": "log_type"
      },
      {
        "type": "text",
        "name": "log_message"
      }
    ]
  },
  "exec": {
    "steps": [
      "Connections/my_mysql",
      {
        "name": "displayLog",
        "module": "dbconnector",
        "action": "paged",
        "options": {
          "connection": "my_mysql",
          "sql": {
            "type": "SELECT",
            "columns": [
              {
                "table": "logging",
                "column": "log_id"
              },
              {
                "table": "logging",
                "column": "log_date"
              },
              {
                "table": "logging",
                "column": "log_type"
              },
              {
                "table": "logging",
                "column": "log_message"
              }
            ],
            "table": {
              "name": "logging"
            },
            "joins": [],
            "orders": [
              {
                "table": "logging",
                "column": "log_id",
                "direction": "DESC"
              }
            ],
            "query": "SELECT log_id, log_date, log_type, log_message\nFROM logging\nORDER BY log_id DESC",
            "params": []
          }
        },
        "output": true,
        "meta": [
          {
            "name": "offset",
            "type": "number"
          },
          {
            "name": "limit",
            "type": "number"
          },
          {
            "name": "total",
            "type": "number"
          },
          {
            "name": "page",
            "type": "object",
            "sub": [
              {
                "name": "offset",
                "type": "object",
                "sub": [
                  {
                    "name": "first",
                    "type": "number"
                  },
                  {
                    "name": "prev",
                    "type": "number"
                  },
                  {
                    "name": "next",
                    "type": "number"
                  },
                  {
                    "name": "last",
                    "type": "number"
                  }
                ]
              },
              {
                "name": "current",
                "type": "number"
              },
              {
                "name": "total",
                "type": "number"
              }
            ]
          },
          {
            "name": "data",
            "type": "array",
            "sub": [
              {
                "name": "log_id",
                "type": "number"
              },
              {
                "name": "log_date",
                "type": "datetime"
              },
              {
                "name": "log_type",
                "type": "text"
              },
              {
                "name": "log_message",
                "type": "text"
              }
            ]
          }
        ],
        "outputType": "object"
      }
    ]
  }
}
JSON
);
?>