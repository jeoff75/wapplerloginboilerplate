<?php
require('../../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "settings": {
    "options": {}
  },
  "meta": {
    "options": {
      "linkedFile": "/app_login.php",
      "linkedForm": "loginForm"
    },
    "$_POST": [
      {
        "type": "number",
        "options": {
          "rules": {
            "core:required": {},
            "core:digits": {}
          }
        },
        "name": "token_2fa"
      },
      {
        "type": "text",
        "options": {
          "rules": {
            "core:required": {},
            "core:alphanumeric": {}
          }
        },
        "name": "login_before_nonce"
      }
    ]
  },
  "exec": {
    "steps": [
      {
        "name": "token_2fa",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{$_POST.token_2fa}}"
        },
        "outputType": "text",
        "output": false
      },
      {
        "name": "login_before_nonce",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{$_POST.login_before_nonce}}"
        },
        "outputType": "text",
        "output": false
      },
      {
        "name": "verify2FAAuthForUser",
        "module": "api",
        "action": "send",
        "options": {
          "url": "https://{{$_SERVER['HTTP_HOST']}}/dmxConnect/api/security/2FA/verifyTwoFactorAuthForUser.php",
          "method": "POST",
          "params": {},
          "schema": [],
          "data": {
            "token_2fa": "{{token_2fa}}",
            "login_before_nonce": "{{login_before_nonce}}"
          }
        },
        "output": false,
        "meta": [
          {
            "type": "object",
            "name": "data",
            "sub": [
              {
                "type": "text",
                "name": "token_2fa"
              },
              {
                "type": "text",
                "name": "error"
              }
            ]
          },
          {
            "type": "object",
            "name": "headers",
            "sub": [
              {
                "type": "text",
                "name": "cache-control"
              },
              {
                "type": "text",
                "name": "cf-cache-status"
              },
              {
                "type": "text",
                "name": "cf-ray"
              },
              {
                "type": "text",
                "name": "content-encoding"
              },
              {
                "type": "text",
                "name": "content-type"
              },
              {
                "type": "text",
                "name": "date"
              },
              {
                "type": "text",
                "name": "expect-ct"
              },
              {
                "type": "text",
                "name": "expires"
              },
              {
                "type": "text",
                "name": "host-header"
              },
              {
                "type": "text",
                "name": "pragma"
              },
              {
                "type": "text",
                "name": "server"
              },
              {
                "type": "text",
                "name": "status"
              },
              {
                "type": "text",
                "name": "x-generator"
              }
            ]
          }
        ],
        "outputType": "object"
      },
      {
        "name": "error",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{verify2FAAuthForUser.data.error}}"
        },
        "outputType": "text",
        "output": false
      },
      {
        "name": "valid_token",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{verify2FAAuthForUser.data.verifyToken.data.valid_token}}"
        },
        "outputType": "text",
        "output": false
      },
      "Connections/my_mysql",
      "SecurityProviders/siteSecurity",
      {
        "name": "",
        "module": "core",
        "action": "condition",
        "options": {
          "if": "{{!error && valid_token}}",
          "then": {
            "steps": [
              "Connections/my_mysql",
              {
                "name": "getUserCredentials",
                "module": "dbconnector",
                "action": "single",
                "options": {
                  "connection": "my_mysql",
                  "sql": {
                    "type": "SELECT",
                    "columns": [
                      {
                        "table": "app_users",
                        "column": "email"
                      },
                      {
                        "table": "app_users",
                        "column": "pass"
                      }
                    ],
                    "table": {
                      "name": "app_users"
                    },
                    "joins": [],
                    "wheres": {
                      "condition": "AND",
                      "rules": [
                        {
                          "id": "app_users.login_before_nonce",
                          "field": "app_users.login_before_nonce",
                          "type": "string",
                          "operator": "equal",
                          "value": "{{login_before_nonce}}",
                          "data": {
                            "table": "app_users",
                            "column": "login_before_nonce",
                            "type": "text"
                          },
                          "operation": "="
                        }
                      ],
                      "conditional": null,
                      "valid": true
                    },
                    "query": "SELECT email, pass\nFROM app_users\nWHERE login_before_nonce = :P1 /* {{login_before_nonce}} */",
                    "params": [
                      {
                        "operator": "equal",
                        "type": "expression",
                        "name": ":P1",
                        "value": "{{login_before_nonce}}"
                      }
                    ]
                  }
                },
                "output": false,
                "meta": [
                  {
                    "name": "email",
                    "type": "text"
                  },
                  {
                    "name": "pass",
                    "type": "text"
                  }
                ],
                "outputType": "object"
              },
              {
                "name": "identity_copy",
                "module": "auth",
                "action": "login",
                "options": {
                  "provider": "siteSecurity",
                  "password": "{{getUserCredentials.pass}}",
                  "username": "{{getUserCredentials.email}}",
                  "remember": "{{false}}"
                },
                "output": false,
                "meta": []
              },
              {
                "name": "logged",
                "module": "core",
                "action": "setvalue",
                "options": {
                  "value": "{{true}}"
                },
                "outputType": "text",
                "output": true
              },
              {
                "name": "",
                "options": {
                  "comment": "Nothing else, user already logged in"
                }
              }
            ]
          },
          "else": {
            "steps": [
              {
                "name": "",
                "module": "auth",
                "action": "logout",
                "options": {
                  "provider": "siteSecurity"
                }
              },
              {
                "name": "",
                "options": {
                  "comment": "Send error - KEEP OUTPUT ON VALUE BELOW"
                }
              },
              {
                "name": "error",
                "module": "core",
                "action": "setvalue",
                "options": {
                  "value": "{{'Invalid or expired token : Login again or contact administrator'}}"
                },
                "outputType": "text",
                "output": true
              }
            ],
            "catch": {
              "name": "error",
              "module": "core",
              "action": "setvalue",
              "options": {
                "value": "{{'Invalid or expired token : Login again or contact administrator'}}"
              },
              "outputType": "text",
              "output": true
            }
          }
        }
      },
      {
        "name": "deleteExpiredNonce",
        "module": "dbupdater",
        "action": "update",
        "options": {
          "connection": "my_mysql",
          "sql": {
            "type": "update",
            "values": [
              {
                "table": "app_users",
                "column": "login_before",
                "type": "number",
                "value": "{{null}}"
              },
              {
                "table": "app_users",
                "column": "login_before_nonce",
                "type": "text",
                "value": "{{null}}"
              }
            ],
            "table": "app_users",
            "wheres": {
              "condition": "AND",
              "rules": [
                {
                  "id": "login_before",
                  "field": "login_before",
                  "type": "double",
                  "operator": "less",
                  "value": "{{TIMESTAMP}}",
                  "data": {
                    "column": "login_before"
                  },
                  "operation": "<"
                }
              ],
              "conditional": null,
              "valid": true
            },
            "query": "UPDATE app_users\nSET login_before = :P1 /* {{null}} */, login_before_nonce = :P2 /* {{null}} */\nWHERE login_before < :P3 /* {{TIMESTAMP}} */",
            "params": [
              {
                "name": ":P1",
                "type": "expression",
                "value": "{{null}}"
              },
              {
                "name": ":P2",
                "type": "expression",
                "value": "{{null}}"
              },
              {
                "operator": "less",
                "type": "expression",
                "name": ":P3",
                "value": "{{TIMESTAMP}}"
              }
            ]
          }
        },
        "meta": [
          {
            "name": "affected",
            "type": "number"
          }
        ]
      }
    ]
  }
}
JSON
);
?>