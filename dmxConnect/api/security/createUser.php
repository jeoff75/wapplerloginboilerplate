<?php
require('../../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "settings": {
    "options": {}
  },
  "meta": {
    "options": {
      "linkedFile": "/app_create_user.php",
      "linkedForm": "createUserForm",
      "autoSync": true
    },
    "$_POST": [
      {
        "type": "text",
        "fieldName": "username",
        "options": {
          "rules": {
            "core:email": {},
            "core:required": {}
          }
        },
        "name": "username"
      },
      {
        "type": "text",
        "fieldName": "password",
        "options": {
          "rules": {
            "core:required": {}
          }
        },
        "name": "password"
      },
      {
        "type": "datetime",
        "name": "registered"
      },
      {
        "type": "text",
        "name": "activation_key"
      },
      {
        "type": "text",
        "name": "firstname"
      },
      {
        "type": "text",
        "name": "lastname"
      },
      {
        "type": "number",
        "fieldName": "role",
        "name": "role"
      },
      {
        "type": "number",
        "name": "status"
      },
      {
        "type": "text",
        "fieldName": "secret_2fa",
        "name": "secret_2fa"
      },
      {
        "type": "boolean",
        "fieldName": "activate_2fa",
        "name": "activate_2fa"
      }
    ]
  },
  "exec": {
    "steps": [
      {
        "name": "login",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{$_POST.username}}"
        },
        "outputType": "text"
      },
      {
        "name": "password",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{$_POST.password}}"
        },
        "outputType": "text"
      },
      "Connections/my_mysql",
      {
        "name": "getRandomSalt",
        "module": "dbupdater",
        "action": "custom",
        "options": {
          "connection": "my_mysql",
          "sql": {
            "query": "SELECT MD5(RAND()) as salt",
            "params": []
          }
        },
        "output": false,
        "meta": [
          {
            "name": "salt",
            "type": "text"
          }
        ],
        "outputType": "array"
      },
      {
        "name": "salt",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{getRandomSalt[0].salt}}"
        },
        "outputType": "text",
        "output": false
      },
      {
        "name": "role",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{$_POST.role}}"
        },
        "outputType": "text"
      },
      {
        "name": "firstname",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "test"
        },
        "outputType": "text"
      },
      {
        "name": "lastname",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "test"
        },
        "outputType": "text"
      },
      {
        "name": "status",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{1}}"
        },
        "outputType": "text"
      },
      {
        "name": "secret_2fa",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{$_POST.secret_2fa}}"
        },
        "outputType": "text"
      },
      {
        "name": "activate_2fa",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{$_POST.activate_2fa}}"
        },
        "outputType": "text",
        "output": false
      },
      {
        "name": "",
        "options": {
          "comment": "Swap clear password against hash"
        }
      },
      {
        "name": "password_salted",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{password.sha512(salt)}}"
        },
        "outputType": "text",
        "output": false
      },
      {
        "name": "userAlreadyExists",
        "module": "validator",
        "action": "validate",
        "options": {
          "data": [
            {
              "name": "userAlreadyExists",
              "value": "{{$_POST.username}}",
              "rules": {
                "db:notexists": {
                  "param": {
                    "connection": "my_mysql",
                    "table": "app_users",
                    "column": "email"
                  },
                  "message": "main_login__email_already_exists_login_label"
                }
              },
              "fieldName": "username"
            }
          ]
        }
      },
      {
        "name": "createUserWithout2FA",
        "module": "dbupdater",
        "action": "insert",
        "options": {
          "connection": "my_mysql",
          "sql": {
            "type": "insert",
            "values": [
              {
                "table": "app_users",
                "column": "email",
                "type": "text",
                "value": "{{login}}"
              },
              {
                "table": "app_users",
                "column": "pass",
                "type": "text",
                "value": "{{password_salted}}"
              },
              {
                "table": "app_users",
                "column": "firstname",
                "type": "text",
                "value": "{{firstname}}"
              },
              {
                "table": "app_users",
                "column": "lastname",
                "type": "text",
                "value": "{{lastname}}"
              },
              {
                "table": "app_users",
                "column": "role",
                "type": "number",
                "value": "{{role}}"
              },
              {
                "table": "app_users",
                "column": "status",
                "type": "number",
                "value": "{{status}}"
              },
              {
                "table": "app_users",
                "column": "salt",
                "type": "text",
                "value": "{{salt}}"
              }
            ],
            "table": "app_users",
            "query": "INSERT INTO app_users\n(email, pass, firstname, lastname, role, status, salt) VALUES (:P1 /* {{login}} */, :P2 /* {{password_salted}} */, :P3 /* {{firstname}} */, :P4 /* {{lastname}} */, :P5 /* {{role}} */, :P6 /* {{status}} */, :P7 /* {{salt}} */)",
            "params": [
              {
                "name": ":P1",
                "type": "expression",
                "value": "{{login}}"
              },
              {
                "name": ":P2",
                "type": "expression",
                "value": "{{password_salted}}"
              },
              {
                "name": ":P3",
                "type": "expression",
                "value": "{{firstname}}"
              },
              {
                "name": ":P4",
                "type": "expression",
                "value": "{{lastname}}"
              },
              {
                "name": ":P5",
                "type": "expression",
                "value": "{{role}}"
              },
              {
                "name": ":P6",
                "type": "expression",
                "value": "{{status}}"
              },
              {
                "name": ":P7",
                "type": "expression",
                "value": "{{salt}}"
              }
            ]
          }
        },
        "meta": [
          {
            "name": "identity",
            "type": "text"
          },
          {
            "name": "affected",
            "type": "number"
          }
        ],
        "output": false
      },
      {
        "name": "",
        "module": "core",
        "action": "condition",
        "options": {
          "if": "{{$_POST.activate_2fa}}",
          "then": {
            "steps": {
              "name": "generateTwoFactorSecretKey",
              "module": "api",
              "action": "send",
              "options": {
                "url": "https://{{$_SERVER['HTTP_HOST']}}/dmxConnect/api/security/2FA/addTwoFactorAuthToUser.php",
                "method": "POST",
                "data": {
                  "app_user_id": "{{createUserWithout2FA.identity}}",
                  "secret_2fa": "{{secret_2fa}}"
                }
              },
              "output": false
            }
          }
        },
        "output": false
      },
      {
        "name": "",
        "options": {
          "comment": "DEMO LIMIT : Keep only 100 freshest users max"
        }
      },
      {
        "name": "listFreshest100Users",
        "module": "dbupdater",
        "action": "custom",
        "options": {
          "connection": "my_mysql",
          "sql": {
            "query": "SELECT app_users.app_user_id from app_users order by app_users.registered DESC LIMIT 100",
            "params": []
          }
        },
        "output": false,
        "meta": [
          {
            "name": "app_user_id",
            "type": "number"
          }
        ],
        "outputType": "array"
      },
      {
        "name": "freshestUsers",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{listFreshest100Users.join(\",\", \"app_user_id\").split(\",\")}}"
        },
        "outputType": "text",
        "output": false
      },
      {
        "name": "deleteUsersOver100",
        "module": "dbupdater",
        "action": "delete",
        "options": {
          "connection": "my_mysql",
          "sql": {
            "type": "delete",
            "table": "app_users",
            "wheres": {
              "condition": "AND",
              "rules": [
                {
                  "id": "app_user_id",
                  "field": "app_user_id",
                  "type": "double",
                  "operator": "not_in",
                  "value": "{{freshestUsers}}",
                  "data": {
                    "column": "app_user_id"
                  },
                  "operation": "NOT IN"
                }
              ],
              "conditional": null,
              "valid": true
            },
            "query": "DELETE\nFROM app_users\nWHERE app_user_id NOT IN ({{freshestUsers}})",
            "params": []
          }
        },
        "meta": [
          {
            "name": "affected",
            "type": "number"
          }
        ],
        "output": false
      }
    ]
  }
}
JSON
);
?>