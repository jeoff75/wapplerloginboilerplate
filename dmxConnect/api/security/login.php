<?php
require('../../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "settings": {
    "options": {}
  },
  "meta": {
    "options": {
      "linkedFile": "/app_login.php",
      "linkedForm": "loginForm"
    },
    "$_POST": [
      {
        "type": "text",
        "fieldName": "username",
        "options": {
          "rules": {
            "core:email": {
              "message": "Merci de renseigner un e-mail valide"
            },
            "core:required": {
              "message": "Merci de renseigner votre identifiant (e-mail)"
            }
          }
        },
        "name": "username"
      },
      {
        "type": "text",
        "fieldName": "password",
        "options": {
          "rules": {
            "core:required": {
              "message": "{{translation.value.main_login__password_label}}"
            }
          }
        },
        "name": "password"
      },
      {
        "type": "number",
        "fieldName": "remember",
        "name": "remember"
      },
      {
        "type": "text",
        "name": "twofactorauth"
      },
      {
        "type": "number",
        "name": "app_user_id"
      }
    ]
  },
  "exec": {
    "steps": [
      {
        "name": "password",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{$_POST.password}}"
        },
        "outputType": "text"
      },
      {
        "name": "username",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{$_POST.username}}"
        },
        "outputType": "text",
        "output": false
      },
      "Connections/my_mysql",
      {
        "name": "getUserSalt",
        "module": "dbconnector",
        "action": "single",
        "options": {
          "connection": "my_mysql",
          "sql": {
            "type": "SELECT",
            "columns": [
              {
                "table": "app_users",
                "column": "salt"
              }
            ],
            "table": {
              "name": "app_users"
            },
            "joins": [],
            "wheres": {
              "condition": "AND",
              "rules": [
                {
                  "id": "app_users.email",
                  "field": "app_users.email",
                  "type": "string",
                  "operator": "equal",
                  "value": "{{username}}",
                  "data": {
                    "table": "app_users",
                    "column": "email",
                    "type": "text"
                  },
                  "operation": "="
                }
              ],
              "conditional": null,
              "valid": true
            },
            "query": "SELECT salt\nFROM app_users\nWHERE email = :P1 /* {{username}} */",
            "params": [
              {
                "operator": "equal",
                "type": "expression",
                "name": ":P1",
                "value": "{{username}}"
              }
            ],
            "orders": []
          }
        },
        "output": false,
        "meta": [
          {
            "name": "salt",
            "type": "text"
          }
        ],
        "outputType": "object"
      },
      {
        "name": "password_salted",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{password.sha512(getUserSalt.salt)}}"
        },
        "outputType": "text"
      },
      {
        "name": "",
        "options": {
          "comment": "Check role granting"
        }
      },
      "SecurityProviders/siteSecurity",
      {
        "name": "has2FA",
        "module": "dbconnector",
        "action": "single",
        "options": {
          "connection": "my_mysql",
          "sql": {
            "type": "SELECT",
            "columns": [
              {
                "table": "app_users",
                "column": "two_factor_auth"
              }
            ],
            "table": {
              "name": "app_users"
            },
            "joins": [],
            "wheres": {
              "condition": "AND",
              "rules": [
                {
                  "id": "app_users.email",
                  "field": "app_users.email",
                  "type": "string",
                  "operator": "equal",
                  "value": "{{username}}",
                  "data": {
                    "table": "app_users",
                    "column": "email",
                    "type": "text"
                  },
                  "operation": "="
                }
              ],
              "conditional": null,
              "valid": true
            },
            "query": "SELECT two_factor_auth\nFROM app_users\nWHERE email = :P1 /* {{username}} */",
            "params": [
              {
                "operator": "equal",
                "type": "expression",
                "name": ":P1",
                "value": "{{username}}"
              }
            ]
          }
        },
        "output": false,
        "meta": [
          {
            "name": "two_factor_auth",
            "type": "text"
          }
        ],
        "outputType": "object"
      },
      {
        "name": "",
        "module": "core",
        "action": "condition",
        "options": {
          "if": "{{has2FA.two_factor_auth}}",
          "else": {
            "steps": [
              {
                "name": "identity",
                "module": "auth",
                "action": "login",
                "options": {
                  "provider": "siteSecurity",
                  "password": "{{password_salted}}"
                },
                "output": false,
                "meta": []
              },
              {
                "name": "",
                "options": {
                  "comment": "Nothing else, user already logged in"
                }
              }
            ]
          },
          "then": {
            "steps": [
              {
                "name": "",
                "module": "auth",
                "action": "logout",
                "options": {
                  "provider": "siteSecurity"
                }
              },
              {
                "name": "login_before",
                "module": "core",
                "action": "setvalue",
                "options": {
                  "value": "{{TIMESTAMP+60}}"
                },
                "outputType": "text",
                "output": true
              },
              {
                "name": "login_before_nonce",
                "module": "core",
                "action": "setvalue",
                "options": {
                  "value": "{{TIMESTAMP.sha512(getUserSalt.salt)}}"
                },
                "outputType": "text",
                "output": true
              },
              {
                "name": "mandatory_2fa",
                "module": "core",
                "action": "setvalue",
                "options": {
                  "value": "{{true}}"
                },
                "outputType": "text",
                "output": true
              },
              {
                "name": "error",
                "module": "core",
                "action": "setvalue",
                "options": {
                  "value": "{{'2FA is mandatory'}}"
                },
                "outputType": "text",
                "output": true
              },
              {
                "name": "set2FATimeFrame",
                "module": "dbupdater",
                "action": "update",
                "options": {
                  "connection": "my_mysql",
                  "sql": {
                    "type": "update",
                    "values": [
                      {
                        "table": "app_users",
                        "column": "login_before",
                        "type": "datetime",
                        "value": "{{login_before}}"
                      },
                      {
                        "table": "app_users",
                        "column": "login_before_nonce",
                        "type": "text",
                        "value": "{{login_before_nonce}}"
                      }
                    ],
                    "table": "app_users",
                    "wheres": {
                      "condition": "AND",
                      "rules": [
                        {
                          "id": "email",
                          "field": "email",
                          "type": "string",
                          "operator": "equal",
                          "value": "{{username}}",
                          "data": {
                            "column": "email"
                          },
                          "operation": "="
                        }
                      ],
                      "conditional": null,
                      "valid": true
                    },
                    "query": "UPDATE app_users\nSET login_before = :P1 /* {{login_before}} */, login_before_nonce = :P2 /* {{login_before_nonce}} */\nWHERE email = :P3 /* {{username}} */",
                    "params": [
                      {
                        "name": ":P1",
                        "type": "expression",
                        "value": "{{login_before}}"
                      },
                      {
                        "name": ":P2",
                        "type": "expression",
                        "value": "{{login_before_nonce}}"
                      },
                      {
                        "operator": "equal",
                        "type": "expression",
                        "name": ":P3",
                        "value": "{{username}}"
                      }
                    ]
                  }
                },
                "meta": [
                  {
                    "name": "affected",
                    "type": "number"
                  }
                ],
                "output": false
              },
              {
                "name": "",
                "options": {
                  "comment": "Delete expired nonce values"
                }
              },
              {
                "name": "deleteExpiredNonce",
                "module": "dbupdater",
                "action": "update",
                "options": {
                  "connection": "my_mysql",
                  "sql": {
                    "type": "update",
                    "values": [
                      {
                        "table": "app_users",
                        "column": "login_before",
                        "type": "datetime",
                        "value": "{{null}}"
                      },
                      {
                        "table": "app_users",
                        "column": "login_before_nonce",
                        "type": "text",
                        "value": "{{null}}"
                      }
                    ],
                    "table": "app_users",
                    "wheres": {
                      "condition": "AND",
                      "rules": [
                        {
                          "id": "login_before",
                          "field": "login_before",
                          "type": "datetime",
                          "operator": "less",
                          "value": "{{TIMESTAMP}}",
                          "data": {
                            "column": "login_before"
                          },
                          "operation": "<"
                        }
                      ],
                      "conditional": null,
                      "valid": true
                    },
                    "query": "UPDATE app_users\nSET login_before = :P1 /* {{null}} */, login_before_nonce = :P2 /* {{null}} */\nWHERE login_before < :P3 /* {{TIMESTAMP}} */",
                    "params": [
                      {
                        "name": ":P1",
                        "type": "expression",
                        "value": "{{null}}"
                      },
                      {
                        "name": ":P2",
                        "type": "expression",
                        "value": "{{null}}"
                      },
                      {
                        "operator": "less",
                        "type": "expression",
                        "name": ":P3",
                        "value": "{{TIMESTAMP}}"
                      }
                    ]
                  }
                },
                "meta": [
                  {
                    "name": "affected",
                    "type": "number"
                  }
                ]
              }
            ]
          }
        }
      }
    ]
  }
}
JSON
);
?>