<?php
require('../../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "settings": {
    "options": {}
  },
  "meta": {
    "options": {},
    "$_POST": [
      {
        "type": "text",
        "name": "username"
      },
      {
        "type": "text",
        "name": "password"
      },
      {
        "type": "number",
        "name": "remember"
      }
    ]
  },
  "exec": {
    "steps": [
      "Connections/my_mysql",
      "SecurityProviders/siteSecurity",
      {
        "name": "",
        "module": "auth",
        "action": "logout",
        "options": {
          "provider": "siteSecurity"
        }
      }
    ]
  }
}
JSON
);
?>