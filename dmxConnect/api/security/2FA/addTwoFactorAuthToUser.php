<?php
require('../../../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "settings": {
    "options": {}
  },
  "meta": {
    "options": {},
    "$_POST": [
      {
        "type": "number",
        "name": "app_user_id"
      },
      {
        "type": "text",
        "name": "secret_2fa"
      }
    ]
  },
  "exec": {
    "steps": [
      {
        "name": "app_user_id",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{$_POST.app_user_id}}"
        },
        "outputType": "text"
      },
      {
        "name": "secret_2fa",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{$_POST.secret_2fa}}"
        },
        "outputType": "text",
        "output": false
      },
      "Connections/my_mysql",
      {
        "name": "getUserMD5",
        "module": "dbupdater",
        "action": "custom",
        "options": {
          "connection": "my_mysql",
          "sql": {
            "query": "SELECT app_users.app_user_id, MD5(app_users.app_user_id) as md5 FROM app_users\nWHERE app_users.two_factor_auth IS NULL\nAND app_users.app_user_id = :USERID",
            "params": [
              {
                "name": ":USERID",
                "value": "{{app_user_id}}",
                "test": "{{39}}"
              }
            ]
          }
        },
        "output": false,
        "meta": [
          {
            "name": "app_user_id",
            "type": "text"
          },
          {
            "name": "md5",
            "type": "text"
          }
        ],
        "outputType": "array"
      },
      {
        "name": "md5",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{getUserMD5[0].md5}}"
        },
        "outputType": "text",
        "output": false
      },
      {
        "name": "secret_2fa_encrypted",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{secret_2fa.encrypt(md5)}}"
        },
        "outputType": "text",
        "output": false
      },
      {
        "name": "",
        "options": {
          "comment": "We make sure to not delete any current 2FA already in place for this user"
        }
      },
      {
        "name": "updateUser2FA",
        "module": "dbupdater",
        "action": "update",
        "options": {
          "connection": "my_mysql",
          "sql": {
            "type": "update",
            "values": [
              {
                "table": "app_users",
                "column": "two_factor_auth",
                "type": "text",
                "value": "{{secret_2fa_encrypted}}"
              }
            ],
            "table": "app_users",
            "wheres": {
              "condition": "AND",
              "rules": [
                {
                  "id": "app_user_id",
                  "field": "app_user_id",
                  "type": "double",
                  "operator": "equal",
                  "value": "{{getUserMD5[0].app_user_id}}",
                  "data": {
                    "column": "app_user_id"
                  },
                  "operation": "="
                },
                {
                  "id": "two_factor_auth",
                  "field": "two_factor_auth",
                  "type": "string",
                  "operator": "is_null",
                  "value": null,
                  "data": {
                    "column": "two_factor_auth"
                  },
                  "operation": "IS NULL"
                }
              ],
              "conditional": null,
              "valid": true
            },
            "query": "UPDATE app_users\nSET two_factor_auth = :P1 /* {{secret_2fa_encrypted}} */\nWHERE app_user_id = :P2 /* {{getUserMD5[0].app_user_id}} */ AND two_factor_auth IS NULL",
            "params": [
              {
                "name": ":P1",
                "type": "expression",
                "value": "{{secret_2fa_encrypted}}"
              },
              {
                "operator": "equal",
                "type": "expression",
                "name": ":P2",
                "value": "{{getUserMD5[0].app_user_id}}"
              }
            ]
          }
        },
        "meta": [
          {
            "name": "affected",
            "type": "number"
          }
        ]
      }
    ]
  }
}
JSON
);
?>