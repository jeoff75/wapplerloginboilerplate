<?php
require('../../../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "settings": {
    "options": {}
  },
  "meta": {
    "options": {},
    "$_POST": [
      {
        "type": "number",
        "name": "app_user_id"
      },
      {
        "type": "text",
        "name": "key_2fa"
      }
    ]
  },
  "exec": {
    "steps": [
      {
        "name": "app_user_id",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{39}}"
        },
        "outputType": "text"
      },
      {
        "name": "key_2fa",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "RVP4H7TLRTRJ4KAKGJTUXKRP"
        },
        "outputType": "text",
        "output": true
      },
      "Connections/my_mysql",
      {
        "name": "delete2FA",
        "module": "dbupdater",
        "action": "update",
        "options": {
          "connection": "my_mysql",
          "sql": {
            "type": "update",
            "values": [
              {
                "table": "app_users",
                "column": "two_factor_auth",
                "type": "text",
                "value": "{{null}}"
              }
            ],
            "table": "app_users",
            "wheres": {
              "condition": "AND",
              "rules": [
                {
                  "id": "app_user_id",
                  "field": "app_user_id",
                  "type": "double",
                  "operator": "equal",
                  "value": "{{app_user_id}}",
                  "data": {
                    "column": "app_user_id"
                  },
                  "operation": "="
                }
              ],
              "conditional": null,
              "valid": true
            },
            "query": "UPDATE app_users\nSET two_factor_auth = :P1 /* {{null}} */\nWHERE app_user_id = :P2 /* {{app_user_id}} */",
            "params": [
              {
                "name": ":P1",
                "type": "expression",
                "value": "{{null}}"
              },
              {
                "operator": "equal",
                "type": "expression",
                "name": ":P2",
                "value": "{{app_user_id}}"
              }
            ]
          }
        },
        "meta": [
          {
            "name": "affected",
            "type": "number"
          }
        ]
      },
      {
        "name": "getUserID",
        "module": "dbupdater",
        "action": "custom",
        "options": {
          "connection": "my_mysql",
          "sql": {
            "query": "SELECT app_users.app_user_id, MD5(app_users.app_user_id) as md5 FROM app_users\nWHERE app_users.two_factor_auth IS NULL\nAND app_users.app_user_id = :USERID",
            "params": [
              {
                "name": ":USERID",
                "value": "{{app_user_id}}",
                "test": "{{39}}"
              }
            ]
          }
        },
        "output": true,
        "meta": [
          {
            "name": "app_user_id",
            "type": "text"
          },
          {
            "name": "md5",
            "type": "text"
          }
        ],
        "outputType": "array"
      },
      {
        "name": "md5",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{getUserID[0].md5}}"
        },
        "outputType": "text",
        "output": true
      },
      {
        "name": "key_2fa_encrypted",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{key_2fa.encrypt(md5)}}"
        },
        "outputType": "text",
        "output": true
      },
      {
        "name": "updateUser2FA",
        "module": "dbupdater",
        "action": "update",
        "options": {
          "connection": "my_mysql",
          "sql": {
            "type": "update",
            "values": [
              {
                "table": "app_users",
                "column": "two_factor_auth",
                "type": "text",
                "value": "{{key_2fa_encrypted}}"
              }
            ],
            "table": "app_users",
            "wheres": {
              "condition": "AND",
              "rules": [
                {
                  "id": "app_user_id",
                  "field": "app_user_id",
                  "type": "double",
                  "operator": "equal",
                  "value": "{{getUserID[0].app_user_id}}",
                  "data": {
                    "column": "app_user_id"
                  },
                  "operation": "="
                },
                {
                  "id": "two_factor_auth",
                  "field": "two_factor_auth",
                  "type": "string",
                  "operator": "is_null",
                  "value": null,
                  "data": {
                    "column": "two_factor_auth"
                  },
                  "operation": "IS NULL"
                }
              ],
              "conditional": null,
              "valid": true
            },
            "query": "UPDATE app_users\nSET two_factor_auth = :P1 /* {{key_2fa_encrypted}} */\nWHERE app_user_id = :P2 /* {{getUserID[0].app_user_id}} */ AND two_factor_auth IS NULL",
            "params": [
              {
                "name": ":P1",
                "type": "expression",
                "value": "{{key_2fa_encrypted}}"
              },
              {
                "operator": "equal",
                "type": "expression",
                "name": ":P2",
                "value": "{{getUserID[0].app_user_id}}"
              }
            ]
          }
        },
        "meta": [
          {
            "name": "affected",
            "type": "number"
          }
        ]
      },
      {
        "name": "get2FA",
        "module": "dbconnector",
        "action": "single",
        "options": {
          "connection": "my_mysql",
          "sql": {
            "type": "SELECT",
            "columns": [
              {
                "table": "app_users",
                "column": "app_user_id"
              },
              {
                "table": "app_users",
                "column": "two_factor_auth"
              }
            ],
            "table": {
              "name": "app_users"
            },
            "joins": [],
            "wheres": {
              "condition": "AND",
              "rules": [
                {
                  "condition": "AND",
                  "rules": [
                    {
                      "id": "app_users.app_user_id",
                      "field": "app_users.app_user_id",
                      "type": "double",
                      "operator": "equal",
                      "value": "{{app_user_id}}",
                      "data": {
                        "table": "app_users",
                        "column": "app_user_id",
                        "type": "number"
                      },
                      "operation": "="
                    }
                  ],
                  "conditional": null
                }
              ],
              "conditional": null,
              "valid": true
            },
            "query": "SELECT app_user_id, two_factor_auth\nFROM app_users\nWHERE (app_user_id = :P1 /* {{app_user_id}} */)",
            "params": [
              {
                "operator": "equal",
                "type": "expression",
                "name": ":P1",
                "value": "{{app_user_id}}"
              }
            ],
            "orders": []
          }
        },
        "output": true,
        "meta": [
          {
            "name": "app_user_id",
            "type": "number"
          },
          {
            "name": "two_factor_auth",
            "type": "text"
          }
        ],
        "outputType": "object"
      },
      {
        "name": "key_2fa_decrypted",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{get2FA.two_factor_auth.decrypt(md5)}}"
        },
        "outputType": "text",
        "output": true
      }
    ]
  }
}
JSON
);
?>