<?php
require('../../../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "settings": {
    "options": {}
  },
  "meta": {
    "options": {},
    "$_POST": [
      {
        "type": "number",
        "name": "token_2fa"
      },
      {
        "type": "text",
        "name": "login_before_nonce"
      }
    ]
  },
  "exec": {
    "steps": [
      {
        "name": "login_before_nonce",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{'azerty'}}"
        },
        "outputType": "text",
        "disabled": true
      },
      {
        "name": "login_before_nonce",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{$_POST.login_before_nonce}}"
        },
        "outputType": "text",
        "output": false
      },
      {
        "name": "token_2fa",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{371544}}"
        },
        "outputType": "text",
        "output": false,
        "disabled": true
      },
      {
        "name": "token_2fa",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{$_POST.token_2fa}}"
        },
        "outputType": "text",
        "output": false
      },
      "Connections/my_mysql",
      {
        "name": "get2FAKey",
        "module": "dbupdater",
        "action": "custom",
        "options": {
          "connection": "my_mysql",
          "sql": {
            "query": "SELECT \napp_users.app_user_id, \nMD5(app_users.app_user_id) as md5,\napp_users.two_factor_auth\nFROM app_users\nWHERE\napp_users.login_before >= :LOGINBEFORE AND\napp_users.login_before_nonce = :LOGINBEFORENONCE",
            "params": [
              {
                "name": ":LOGINBEFORE",
                "value": "{{TIMESTAMP}}",
                "test": "{{TIMESTAMP}}"
              },
              {
                "name": ":LOGINBEFORENONCE",
                "value": "{{login_before_nonce}}",
                "test": "{{'azerty'}}"
              }
            ]
          }
        },
        "output": false,
        "meta": [
          {
            "name": "app_user_id",
            "type": "text"
          },
          {
            "name": "md5",
            "type": "text"
          },
          {
            "name": "two_factor_auth",
            "type": "text"
          }
        ],
        "outputType": "array"
      },
      {
        "name": "",
        "module": "core",
        "action": "condition",
        "options": {
          "if": "{{get2FAKey}}",
          "else": {
            "steps": [
              {
                "name": "",
                "options": {
                  "comment": "Send error - KEEP OUTPUT ON VALUE BELOW"
                }
              },
              {
                "name": "error",
                "module": "core",
                "action": "setvalue",
                "options": {
                  "value": "{{'2FA authentication failed : Code invalid or expired'"
                },
                "outputType": "text",
                "output": true
              }
            ]
          },
          "then": {
            "steps": [
              {
                "name": "md5",
                "module": "core",
                "action": "setvalue",
                "options": {
                  "value": "{{get2FAKey[0].md5}}"
                },
                "outputType": "text",
                "output": false
              },
              {
                "name": "secret_2fa_decrypted",
                "module": "core",
                "action": "setvalue",
                "options": {
                  "value": "{{get2FAKey[0].two_factor_auth.decrypt(md5)}}"
                },
                "outputType": "text",
                "output": false
              },
              {
                "name": "",
                "options": {
                  "comment": "Check 2FA token validity - KEEP OUTPUT ON API ACTION  BELOW"
                }
              },
              {
                "name": "verifyToken",
                "module": "api",
                "action": "send",
                "options": {
                  "url": "https://{{$_SERVER['HTTP_HOST']}}/extlib/phpotp/verifyToken.php",
                  "method": "POST",
                  "data": {
                    "token_2fa": "{{token_2fa}}",
                    "secret_2fa": "{{secret_2fa_decrypted}}"
                  },
                  "schema": []
                },
                "output": true,
                "meta": [
                  {
                    "type": "object",
                    "name": "data",
                    "sub": [
                      {
                        "type": "boolean",
                        "name": "valid_token"
                      },
                      {
                        "type": "text",
                        "name": "current_token"
                      },
                      {
                        "type": "text",
                        "name": "passed_secret"
                      },
                      {
                        "type": "number",
                        "name": "passed_token"
                      }
                    ]
                  },
                  {
                    "type": "object",
                    "name": "headers",
                    "sub": [
                      {
                        "type": "text",
                        "name": "cf-cache-status"
                      },
                      {
                        "type": "text",
                        "name": "cf-ray"
                      },
                      {
                        "type": "text",
                        "name": "content-encoding"
                      },
                      {
                        "type": "text",
                        "name": "content-type"
                      },
                      {
                        "type": "text",
                        "name": "date"
                      },
                      {
                        "type": "text",
                        "name": "expect-ct"
                      },
                      {
                        "type": "text",
                        "name": "host-header"
                      },
                      {
                        "type": "text",
                        "name": "server"
                      },
                      {
                        "type": "text",
                        "name": "status"
                      }
                    ]
                  }
                ],
                "outputType": "object"
              }
            ]
          }
        },
        "output": false
      }
    ]
  }
}
JSON
);
?>