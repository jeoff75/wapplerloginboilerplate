<?php
require('../../../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "settings": {
    "options": {}
  },
  "meta": {
    "options": {}
  },
  "exec": {
    "steps": [
      {
        "name": "data",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{'mYsEcReT'}}"
        },
        "outputType": "text",
        "output": true
      },
      {
        "name": "md5",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{''.md5(\"MySalt\")}}"
        },
        "outputType": "text",
        "output": true
      },
      {
        "name": "encoded",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{data.encrypt(md5)}}"
        },
        "outputType": "text",
        "output": true
      },
      {
        "name": "decoded",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{encoded.decrypt(md5)}}"
        },
        "outputType": "text",
        "output": true
      }
    ]
  }
}
JSON
);
?>