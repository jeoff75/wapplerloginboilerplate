<?php
require('../../../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "settings": {
    "options": {}
  },
  "meta": {
    "options": {},
    "$_POST": [
      {
        "type": "number",
        "name": "app_user_id"
      },
      {
        "type": "text",
        "name": "key_2fa"
      }
    ]
  },
  "exec": {
    "steps": {
      "name": "getNew2FASecretKey",
      "module": "api",
      "action": "send",
      "options": {
        "url": "https://{{$_SERVER['HTTP_HOST']}}/extlib/phpotp/generateSecret.php",
        "schema": []
      },
      "output": true,
      "meta": [
        {
          "type": "object",
          "name": "data",
          "sub": [
            {
              "type": "text",
              "name": "secret_2fa"
            },
            {
              "type": "text",
              "name": "qr_code"
            }
          ]
        },
        {
          "type": "object",
          "name": "headers",
          "sub": [
            {
              "type": "text",
              "name": "cf-cache-status"
            },
            {
              "type": "text",
              "name": "cf-ray"
            },
            {
              "type": "text",
              "name": "content-encoding"
            },
            {
              "type": "text",
              "name": "content-type"
            },
            {
              "type": "text",
              "name": "date"
            },
            {
              "type": "text",
              "name": "expect-ct"
            },
            {
              "type": "text",
              "name": "host-header"
            },
            {
              "type": "text",
              "name": "server"
            },
            {
              "type": "text",
              "name": "status"
            },
            {
              "type": "text",
              "name": "x-proxy-cache"
            }
          ]
        }
      ],
      "outputType": "object"
    }
  }
}
JSON
);
?>