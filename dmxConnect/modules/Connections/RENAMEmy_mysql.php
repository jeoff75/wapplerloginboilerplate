<?php
// Database Type : "MySQL"
// Database Adapter : "mysql"
$exports = <<<'JSON'
{
    "name": "my_mysql",
    "module": "dbconnector",
    "action": "connect",
    "options": {
        "server": "mysql",
        "connectionString": "mysql:host=127.0.0.1;sslverify=false;port=3306;dbname=loginboilerplate;user=loginbo;password=test;charset=utf8",
        "limit" : 1000,
        "debug" : false,
        "meta"  : {"allTables":["app_config","app_users","logging","purchases","translations"],"allViews":[],"tables":{"translations":{"columns":{"trans_id":{"type":"int","primary":true},"text_key":{"type":"varchar","size":255},"lang_key":{"type":"varchar","size":255},"translation":{"type":"varchar","size":255}}},"customers_root":{"columns":[]},"customers_info":{"columns":[]},"pp_customers":{"columns":[]},"Select Table":{"columns":[]},"logging":{"columns":{"log_id":{"type":"int","primary":true},"log_date":{"type":"datetime","defaultValue":"CURRENT_TIMESTAMP"},"log_type":{"type":"varchar","size":10},"log_message":{"type":"text","size":65535}}},"app_users":{"columns":{"app_user_id":{"type":"bigint","primary":true},"email":{"type":"varchar","size":100},"pass":{"type":"varchar","size":64},"registered":{"type":"datetime","nullable":true,"defaultValue":"CURRENT_TIMESTAMP"},"activation_key":{"type":"varchar","size":60,"nullable":true},"firstname":{"type":"varchar","size":100},"lastname":{"type":"varchar","size":100},"role":{"type":"tinyint"},"status":{"type":"tinyint"}}},"app_config":{"columns":{"id":{"type":"int","primary":true},"setting_name":{"type":"varchar","size":255},"setting_value":{"type":"varchar","size":255}}}}}
    }
}
JSON;
?>