<?php
$exports = <<<'JSON'
{
  "name": "find_translation",
  "module": "dbconnector",
  "action": "select",
  "options": {
    "connection": "Translations",
    "sql": {
      "type": "SELECT",
      "columns": [
        {
          "table": "translations",
          "column": "translation"
        }
      ],
      "table": {
        "name": "translations"
      },
      "joins": [],
      "wheres": null,
      "orders": [],
      "query": "SELECT translation\nFROM translations",
      "params": []
    },
    "test": true
  },
  "output": true,
  "meta": [
    {
      "name": "translation",
      "type": "text"
    }
  ],
  "outputType": "array"
}
JSON;
?>