<?php
$exports = <<<'JSON'
{
  "name": "siteSecurity",
  "module": "auth",
  "action": "provider",
  "options": {
    "secret": "MOxZIRSWr9BwoEbp6rT",
    "provider": "Database",
    "connection": "my_mysql",
    "users": {
      "table": "app_users",
      "identity": "app_user_id",
      "username": "email",
      "password": "pass"
    },
    "permissions": {
      "superadmin": {
        "table": "app_users",
        "identity": "app_user_id",
        "conditions": [
          {
            "column": "role",
            "operator": ">=",
            "value": "{{32}}"
          },
          {
            "column": "status",
            "operator": "=",
            "value": "{{1}}"
          }
        ]
      },
      "administrator": {
        "table": "app_users",
        "identity": "app_user_id",
        "conditions": [
          {
            "column": "role",
            "operator": ">=",
            "value": "{{16}}"
          },
          {
            "column": "status",
            "operator": "=",
            "value": "{{1}}"
          }
        ]
      },
      "developer": {
        "table": "app_users",
        "identity": "app_user_id",
        "conditions": [
          {
            "column": "role",
            "operator": ">=",
            "value": "{{8}}"
          },
          {
            "column": "status",
            "operator": "=",
            "value": "{{1}}"
          }
        ]
      },
      "analyst": {
        "table": "app_users",
        "identity": "app_user_id",
        "conditions": [
          {
            "column": "role",
            "operator": ">=",
            "value": "{{4}}"
          },
          {
            "column": "status",
            "operator": "=",
            "value": "{{1}}"
          }
        ]
      },
      "support": {
        "table": "app_users",
        "identity": "app_user_id",
        "conditions": [
          {
            "column": "role",
            "operator": ">=",
            "value": "{{2}}"
          },
          {
            "column": "status",
            "operator": "=",
            "value": "{{1}}"
          }
        ]
      },
      "viewer": {
        "table": "app_users",
        "identity": "app_user_id",
        "conditions": [
          {
            "column": "role",
            "operator": ">=",
            "value": "{{1}}"
          },
          {
            "column": "status",
            "operator": "=",
            "value": "{{1}}"
          }
        ]
      },
      "unconfirmed": {
        "table": "app_users",
        "identity": "app_user_id",
        "conditions": [
          {
            "column": "status",
            "operator": "=",
            "value": "{{0}}"
          }
        ]
      }
    }
  },
  "meta": [
    {
      "name": "identity",
      "type": "text"
    }
  ]
}
JSON;
?>