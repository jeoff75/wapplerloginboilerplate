<?php
require('dmxConnectLib/dmxConnect.php');

$app = new \lib\App();

$app->exec(<<<'JSON'
{
	"steps": [
		"Connections/my_mysql",
		"SecurityProviders/siteSecurity",
		{
			"module": "auth",
			"action": "restrict",
			"options": {"permissions":"viewer","loginUrl":"app_login.php","forbiddenUrl":"app_confirm.php","provider":"siteSecurity"}
		}
	]
}
JSON
, TRUE);
?>
<!doctype html>
<html><head>
  <meta charset="UTF-8">
  <title>Untitled Document</title>
  <script src="dmxAppConnect/dmxAppConnect.js"></script>
  <script src="js/jquery-3.3.1.slim.min.js"></script>
  <link rel="stylesheet" href="fontawesome4/css/font-awesome.min.css">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="bootstrap/4/css/bootstrap.min.css">
  <script src="dmxAppConnect/dmxBootstrap4Navigation/dmxBootstrap4Navigation.js" defer=""></script>
  <script src="dmxAppConnect/dmxBootstrap4Collapse/dmxBootstrap4Collapse.js" defer=""></script>
  <style>
    
  </style>
  <script src="dmxAppConnect/dmxFormatter/dmxFormatter.js" defer=""></script>
  <script src="dmxAppConnect/dmxBrowser/dmxBrowser.js" defer=""></script>
  </head>
  <body is="dmx-app" id="app_menu">
    <div is="dmx-browser" id="browserSession"></div>
    <dmx-serverconnect id="logout" url="dmxConnect/api/security/logout.php" noload="noload" dmx-on:success="browserSession.goto('app_login.php')"></dmx-serverconnect>
    <dmx-serverconnect id="translate" url="dmxConnect/api/database/translate.php" dmx-param:language="browserSession.language.split('-')[0]"></dmx-serverconnect>
    <dmx-value id="translation" dmx-bind:value="translate.data.getTranslation.toKeyedObject('text_key', 'translation')"></dmx-value>    
    <nav class="navbar navbar-expand-md navbar-light bg-light fixed-top" id="menu-top">
      <a class="navbar-brand" href="#">{{translation.value.main_header__brand_label}}</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapse1" aria-controls="collapse1" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div id="collapse1" class="collapse navbar-collapse justify-content-between">
        <ul class="navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="./">{{translation.value.main_header__home_label}}</a>
          </li>
        <li class="nav-item">
          <a class="nav-link" href="#">{{translation.value.main_header__tools_label}}</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">{{translation.value.main_header__pricing_label}}</a>
          </li>
        <li class="nav-item"></li>
        </ul>
        <ul class="navbar-nav">
          <li class="nav-item mr-2 mt-2 mt-md-0">
            <a class="btn btn-outline-danger" href="#" dmx-on:click="logout.load({})">{{translation.value.main_header__logout_label}}</a>
          </li>
        </ul>
      </div>
    </nav>
    <div class="container pt-3 pt-md-5 margin-top" id="cont-app-selection">
      <div class="row px-lg-5">
        <div class="col-12 col-sm-6 mb-2 mb-md-3">
          <button class="btn btn-primary btn-lg btn-block" dmx-on:click="browserSession.goto('app_test_role_superadmin.php')">{{translation.value.app_menu__test_role_superadmin}}<br><i class="fa fa-user"></i></button>
        </div>
        <div class="col-12 col-sm-6 mb-2 mb-md-3">
          <button class="btn btn-outline-success btn-lg btn-block" dmx-on:click="browserSession.goto('app_test_role_admin.php')">{{translation.value.app_menu__test_role_admin}}<br><i class="fa fa-user"></i></button>
        </div>
        <div class="col-12 col-sm-6 mb-2 mb-md-3">
          <button class="btn btn-outline-info btn-lg btn-block" dmx-on:click="browserSession.goto('app_test_role_viewer.php')">{{translation.value.app_menu__test_role_viewer}}<br><i class="fa fa-user"></i></button>
        </div>
        <div class="col-12 col-sm-6 mb-2 mb-md-3">
          <button class="btn btn-outline-warning btn-lg btn-block" dmx-on:click="browserSession.goto('app_test_role_developer.php')">{{translation.value.app_menu__test_role_developer}}<br><i class="fa fa-user"></i></button>
        </div>        
      </div>
    </div>
    <script src="bootstrap/4/js/popper.min.js"></script>
    <script src="bootstrap/4/js/bootstrap.min.js"></script>
    <script>
    $(window).resize(function(){
      $(document.body).css("margin-top", $("#menu-top").height());
    }).resize();
    </script>
  </body></html>

