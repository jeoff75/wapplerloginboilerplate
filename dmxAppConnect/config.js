dmx.config({
  "test": {
    "var_translation": {
      "meta": [
        {
          "name": "text_key",
          "type": "text"
        },
        {
          "name": "translation",
          "type": "text"
        }
      ],
      "outputType": "array"
    }
  },
  "tutorial_translation": {
    "var1": {
      "meta": [
        {
          "name": "text_key",
          "type": "text"
        },
        {
          "name": "translation",
          "type": "text"
        }
      ],
      "outputType": "array"
    }
  },
  "blue": {
    "data_view1": {
      "meta": [
        {
          "name": "Code",
          "type": "text"
        },
        {
          "name": "Mode",
          "type": "text"
        },
        {
          "name": "FirstPurchase",
          "type": "boolean"
        }
      ],
      "outputType": "object"
    },
    "jsonMainProduct": {
      "meta": null,
      "outputType": "array"
    },
    "api1": [
      {
        "type": "object",
        "name": "data",
        "sub": [
          {
            "type": "text",
            "name": "support_email"
          },
          {
            "type": "text",
            "name": "test"
          },
          {
            "type": "text",
            "name": "root_url"
          },
          {
            "type": "key_array",
            "name": "produits",
            "sub": [
              {
                "type": "object",
                "name": "test_mode",
                "sub": [
                  {
                    "type": "text",
                    "name": "stripe"
                  },
                  {
                    "type": "text",
                    "name": "pap"
                  },
                  {
                    "type": "text",
                    "name": "paypal"
                  }
                ]
              },
              {
                "type": "text",
                "name": "plan_id"
              },
              {
                "type": "text",
                "name": "devise"
              },
              {
                "type": "text",
                "name": "montant"
              },
              {
                "type": "text",
                "name": "type"
              },
              {
                "type": "text",
                "name": "1clic_upsell"
              },
              {
                "type": "object",
                "name": "essai",
                "sub": [
                  {
                    "type": "text",
                    "name": "duree"
                  },
                  {
                    "type": "text",
                    "name": "periodicite"
                  },
                  {
                    "type": "text",
                    "name": "montant"
                  }
                ]
              },
              {
                "type": "object",
                "name": "abo",
                "sub": [
                  {
                    "type": "text",
                    "name": "intervalle"
                  },
                  {
                    "type": "text",
                    "name": "periodicite"
                  },
                  {
                    "type": "text",
                    "name": "cycles"
                  }
                ]
              },
              {
                "type": "text",
                "name": "libelle"
              },
              {
                "type": "text",
                "name": "libelle_abo"
              },
              {
                "type": "text",
                "name": "libelle_essai"
              },
              {
                "type": "text",
                "name": "email_du_service_client"
              },
              {
                "type": "text",
                "name": "email_du_service_commercial"
              },
              {
                "type": "text",
                "name": "image_garantie"
              },
              {
                "type": "text",
                "name": "commission"
              },
              {
                "type": "text",
                "name": "URL_cgv"
              },
              {
                "type": "text",
                "name": "URL_achat_confirme"
              },
              {
                "type": "key_array",
                "name": "notification",
                "sub": [
                  {
                    "type": "text",
                    "name": "objet"
                  },
                  {
                    "type": "text",
                    "name": "email_expediteur"
                  },
                  {
                    "type": "text",
                    "name": "nom_expediteur"
                  },
                  {
                    "type": "text",
                    "name": "modele"
                  }
                ]
              },
              {
                "type": "object",
                "name": "creation",
                "sub": [
                  {
                    "type": "object",
                    "name": "zone_membre",
                    "sub": [
                      {
                        "type": "text",
                        "name": "type"
                      },
                      {
                        "type": "text",
                        "name": "niveau"
                      },
                      {
                        "type": "text",
                        "name": "api_url"
                      },
                      {
                        "type": "text",
                        "name": "api_key"
                      },
                      {
                        "type": "text",
                        "name": "login_url"
                      }
                    ]
                  },
                  {
                    "type": "object",
                    "name": "liste_ar",
                    "sub": [
                      {
                        "type": "text",
                        "name": "autorepondeur"
                      },
                      {
                        "type": "text",
                        "name": "autorepondeur_id"
                      },
                      {
                        "type": "text",
                        "name": "abandon"
                      },
                      {
                        "type": "text",
                        "name": "abandon_cycle"
                      },
                      {
                        "type": "text",
                        "name": "client"
                      },
                      {
                        "type": "text",
                        "name": "client_cycle"
                      }
                    ]
                  },
                  {
                    "type": "object",
                    "name": "cpanel_email",
                    "sub": [
                      {
                        "type": "text",
                        "name": "cpanel_id"
                      },
                      {
                        "type": "text",
                        "name": "email_domain"
                      },
                      {
                        "type": "text",
                        "name": "email_quota"
                      }
                    ]
                  }
                ]
              },
              {
                "type": "object",
                "name": "offre",
                "sub": [
                  {
                    "type": "text",
                    "name": "intro"
                  },
                  {
                    "type": "array",
                    "name": "fonctionnalites",
                    "sub": [
                      {
                        "type": "text",
                        "name": "description"
                      }
                    ]
                  }
                ]
              },
              {
                "type": "array",
                "name": "temoignages",
                "sub": [
                  {
                    "type": "text",
                    "name": "citation"
                  },
                  {
                    "type": "text",
                    "name": "auteur"
                  }
                ]
              },
              {
                "type": "object",
                "name": "aide_a_la_commande",
                "sub": [
                  {
                    "type": "text",
                    "name": "image"
                  },
                  {
                    "type": "text",
                    "name": "message"
                  }
                ]
              }
            ]
          },
          {
            "type": "text",
            "name": "produit_par_defaut"
          }
        ]
      },
      {
        "type": "object",
        "name": "headers",
        "sub": [
          {
            "type": "text",
            "name": "date"
          },
          {
            "type": "text",
            "name": "content-encoding"
          },
          {
            "type": "text",
            "name": "server"
          },
          {
            "type": "text",
            "name": "expect-ct"
          },
          {
            "type": "text",
            "name": "content-type"
          },
          {
            "type": "text",
            "name": "status"
          },
          {
            "type": "text",
            "name": "host-header"
          },
          {
            "type": "text",
            "name": "cf-ray"
          },
          {
            "type": "text",
            "name": "x-proxy-cache"
          }
        ]
      }
    ],
    "jsonCartConfig": [
      {
        "type": "object",
        "name": "data",
        "sub": [
          {
            "type": "object",
            "name": "test_mode",
            "sub": [
              {
                "type": "text",
                "name": "stripe"
              },
              {
                "type": "text",
                "name": "pap"
              },
              {
                "type": "text",
                "name": "paypal"
              }
            ]
          },
          {
            "type": "text",
            "name": "plan_id"
          },
          {
            "type": "text",
            "name": "devise"
          },
          {
            "type": "text",
            "name": "montant"
          },
          {
            "type": "text",
            "name": "type"
          },
          {
            "type": "text",
            "name": "1clic_upsell"
          },
          {
            "type": "object",
            "name": "essai",
            "sub": [
              {
                "type": "text",
                "name": "duree"
              },
              {
                "type": "text",
                "name": "periodicite"
              },
              {
                "type": "text",
                "name": "montant"
              }
            ]
          },
          {
            "type": "object",
            "name": "abo",
            "sub": [
              {
                "type": "text",
                "name": "intervalle"
              },
              {
                "type": "text",
                "name": "periodicite"
              },
              {
                "type": "text",
                "name": "cycles"
              }
            ]
          },
          {
            "type": "text",
            "name": "libelle"
          },
          {
            "type": "text",
            "name": "libelle_abo"
          },
          {
            "type": "text",
            "name": "libelle_essai"
          },
          {
            "type": "text",
            "name": "email_du_service_client"
          },
          {
            "type": "text",
            "name": "email_du_service_commercial"
          },
          {
            "type": "text",
            "name": "image_garantie"
          },
          {
            "type": "text",
            "name": "commission"
          },
          {
            "type": "text",
            "name": "URL_cgv"
          },
          {
            "type": "text",
            "name": "URL_achat_confirme"
          },
          {
            "type": "key_array",
            "name": "notification",
            "sub": [
              {
                "type": "text",
                "name": "objet"
              },
              {
                "type": "text",
                "name": "email_expediteur"
              },
              {
                "type": "text",
                "name": "nom_expediteur"
              },
              {
                "type": "text",
                "name": "modele"
              }
            ]
          },
          {
            "type": "object",
            "name": "creation",
            "sub": [
              {
                "type": "object",
                "name": "zone_membre",
                "sub": [
                  {
                    "type": "text",
                    "name": "type"
                  },
                  {
                    "type": "text",
                    "name": "niveau"
                  },
                  {
                    "type": "text",
                    "name": "api_url"
                  },
                  {
                    "type": "text",
                    "name": "api_key"
                  },
                  {
                    "type": "text",
                    "name": "login_url"
                  }
                ]
              },
              {
                "type": "object",
                "name": "liste_ar",
                "sub": [
                  {
                    "type": "text",
                    "name": "autorepondeur"
                  },
                  {
                    "type": "text",
                    "name": "autorepondeur_id"
                  },
                  {
                    "type": "text",
                    "name": "abandon"
                  },
                  {
                    "type": "text",
                    "name": "abandon_cycle"
                  },
                  {
                    "type": "text",
                    "name": "client"
                  },
                  {
                    "type": "text",
                    "name": "client_cycle"
                  }
                ]
              },
              {
                "type": "object",
                "name": "cpanel_email",
                "sub": [
                  {
                    "type": "text",
                    "name": "cpanel_id"
                  },
                  {
                    "type": "text",
                    "name": "email_domain"
                  },
                  {
                    "type": "text",
                    "name": "email_quota"
                  }
                ]
              }
            ]
          },
          {
            "type": "object",
            "name": "offre",
            "sub": [
              {
                "type": "text",
                "name": "intro"
              },
              {
                "type": "array",
                "name": "fonctionnalites",
                "sub": [
                  {
                    "type": "text",
                    "name": "description"
                  }
                ]
              }
            ]
          },
          {
            "type": "array",
            "name": "temoignages",
            "sub": [
              {
                "type": "text",
                "name": "citation"
              },
              {
                "type": "text",
                "name": "auteur"
              }
            ]
          },
          {
            "type": "object",
            "name": "aide_a_la_commande",
            "sub": [
              {
                "type": "text",
                "name": "image"
              },
              {
                "type": "text",
                "name": "message"
              }
            ]
          }
        ]
      },
      {
        "type": "object",
        "name": "headers",
        "sub": [
          {
            "type": "text",
            "name": "date"
          },
          {
            "type": "text",
            "name": "content-encoding"
          },
          {
            "type": "text",
            "name": "server"
          },
          {
            "type": "text",
            "name": "expect-ct"
          },
          {
            "type": "text",
            "name": "content-type"
          },
          {
            "type": "text",
            "name": "status"
          },
          {
            "type": "text",
            "name": "host-header"
          },
          {
            "type": "text",
            "name": "cf-ray"
          },
          {
            "type": "text",
            "name": "x-proxy-cache"
          }
        ]
      }
    ],
    "bulletPoints": {
      "meta": [
        {
          "type": "text",
          "name": "description"
        }
      ],
      "outputType": "array"
    },
    "testimonials": {
      "meta": [
        {
          "type": "text",
          "name": "citation"
        },
        {
          "type": "text",
          "name": "auteur"
        }
      ],
      "outputType": "array"
    },
    "decimalAmount": {
      "outputType": "number"
    },
    "jsonTvaIntra": [
      {
        "type": "object",
        "name": "data",
        "sub": [
          {
            "type": "text",
            "name": "vat_rate"
          },
          {
            "type": "text",
            "name": "vat_message"
          }
        ]
      },
      {
        "type": "object",
        "name": "headers",
        "sub": [
          {
            "type": "text",
            "name": "date"
          },
          {
            "type": "text",
            "name": "content-encoding"
          },
          {
            "type": "text",
            "name": "server"
          },
          {
            "type": "text",
            "name": "expect-ct"
          },
          {
            "type": "text",
            "name": "content-type"
          },
          {
            "type": "text",
            "name": "status"
          },
          {
            "type": "text",
            "name": "host-header"
          },
          {
            "type": "text",
            "name": "cf-ray"
          },
          {
            "type": "text",
            "name": "x-proxy-cache"
          }
        ]
      }
    ],
    "query": [
      {
        "type": "text",
        "name": "product"
      },
      {
        "type": "text",
        "name": "hop"
      }
    ],
    "var1": {
      "meta": null,
      "outputType": "text"
    }
  },
  "learnybox_test_commentaire": {
    "repeat1": {
      "meta": [
        {
          "name": "id",
          "type": "text"
        },
        {
          "name": "auteur",
          "type": "text"
        },
        {
          "name": "email",
          "type": "text"
        },
        {
          "name": "commentaire",
          "type": "text"
        }
      ],
      "outputType": "array"
    }
  },
  "lastlogs": {
    "repeat1": {
      "meta": [
        {
          "name": "log_id",
          "type": "number"
        },
        {
          "name": "log_date",
          "type": "datetime"
        },
        {
          "name": "log_type",
          "type": "text"
        },
        {
          "name": "log_message",
          "type": "text"
        }
      ],
      "outputType": "array"
    },
    "repeat2": {
      "meta": [
        {
          "name": "offset",
          "type": "object",
          "sub": [
            {
              "name": "first",
              "type": "number"
            },
            {
              "name": "prev",
              "type": "number"
            },
            {
              "name": "next",
              "type": "number"
            },
            {
              "name": "last",
              "type": "number"
            }
          ]
        },
        {
          "name": "current",
          "type": "number"
        },
        {
          "name": "total",
          "type": "number"
        }
      ],
      "outputType": "object"
    }
  },
  "main_app_selection": {
    "translation": {
      "meta": [
        {
          "name": "text_key",
          "type": "text"
        },
        {
          "name": "translation",
          "type": "text"
        }
      ],
      "outputType": "array"
    }
  },
  "app_login": {
    "status_2fa": {
      "meta": null,
      "outputType": "text"
    }
  }
});
