<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>Votre Ange Gardien protège vos revenus en ligne ! :)</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="css/ventes_online.css">
	<script src="dmxAppConnect/dmxAppConnect.js"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT" crossorigin="anonymous"></script>
	<script src="dmxAppConnect/dmxBootstrap4Navigation/dmxBootstrap4Navigation.js" defer=""></script>
	<script src="dmxAppConnect/dmxBootstrap4Collapse/dmxBootstrap4Collapse.js" defer=""></script>
	<style>
	</style>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.0/css/all.css" integrity="sha384-REHJTs1r2ErKBuJB0fCK99gCYsVjwxHrSU0N7I1zl9vZbggVJXRMsv/sLlOAGb4M" crossorigin="anonymous">
	<script src="dmxAppConnect/dmxFormatter/dmxFormatter.js" defer=""></script>
	<script src="dmxAppConnect/dmxBrowser/dmxBrowser.js" defer=""></script>
	<link rel="stylesheet" href="dmxAppConnect/dmxValidator/dmxValidator.css">
	<script src="dmxAppConnect/dmxValidator/dmxValidator.js" defer=""></script>
</head>

<body is="dmx-app" id="app_create_user">
	<div is="dmx-browser" id="browserSession"></div>
	<dmx-serverconnect id="translate" url="dmxConnect/api/database/translate.php" dmx-param:language="browserSession.language.split('-')[0]"></dmx-serverconnect>
	<dmx-serverconnect id="generate2FASecret" url="dmxConnect/api/security/2FA/generateTwoFactorSecretKey.php" dmx-param:language="browserSession.language.split('-')[0]"></dmx-serverconnect>
	<dmx-value id="translation" dmx-bind:value="translate.data.getTranslation.toKeyedObject('text_key', 'translation')"></dmx-value>
	<nav class="navbar navbar-expand-md navbar-light bg-light fixed-top" id="menu-top">
		<a class="navbar-brand" href="#">{{translation.value.main_header__brand_label}}</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapse1" aria-controls="collapse1" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div id="collapse1" class="collapse navbar-collapse justify-content-between">
			<ul class="navbar-nav">
				<li class="nav-item active">
					<a class="nav-link" href="./">{{translation.value.main_header__home_label}}</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">{{translation.value.main_header__tools_label}}</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">{{translation.value.main_header__pricing_label}}</a>
				</li>
				<li class="nav-item"></li>
			</ul>
			<ul class="navbar-nav">
				<li class="nav-item mr-2 mt-2 mt-md-0">
					<a class="btn btn-outline-success" href="#" dmx-on:click="browserSession.goto('app_menu.php')">{{translation.value.main_header__login_label}}</a>
				</li>
			</ul>
		</div>
	</nav>
	<div class="container pt-3 pt-md-5" id="cont-main-login">
		<div class="row px-lg-5 mb-2 justify-content-sm-around">
			<div class="d-none d-md-block col-md">
			</div>
			<div class="col-12 col-sm-8 col-md-6">
				<form id="createUserForm" method="post" is="dmx-serverconnect-form" action="dmxConnect/api/security/createUser.php" dmx-on:success="browserSession.alert(translation.value.main_user_creation__account_created)"
					dmx-on:error="browserSession.alert(translation.value.main_user_creation__account_not_created)">
					<div class="form-group">
						<label for="username">{{translation.value.main_user_creation__login_label}}</label>
						<input type="email" class="form-control" id="username" aria-describedby="emailHelp" placeholder="Saisissez votre e-mail pour vous connecter" name="username" value="" dmx-bind:required="true"
							dmx-bind:data-msg-required="{{translation.value.main_login__missing_login_label}}" dmx-bind:data-rule-email="true" dmx-bind:data-msg-email="{{translation.value.main_login__not_an_email_login_label}}">
					</div>
					<div class="form-group">
						<label for="password">{{translation.value.main_user_creation__password_label}}</label>
						<input type="password" class="form-control" id="password" placeholder="Saisissez votre mot de passe" name="password" value="" dmx-bind:required="true"
							dmx-bind:data-msg-required="{{translation.value.main_login__missing_password_label}}">
					</div>
					<div class="form-group">
						<label for="role">{{translation.value.main_user_creation__role_label}}</label>
						<select id="role" class="form-control" name="role">
							<option value="32">{{translation.value.app_role__superadmin_label}}</option>
							<option value="16">{{translation.value.app_role__admin_label}}</option>
							<option value="8">{{translation.value.app_role__developer_label}}</option>
							<option value="4">{{translation.value.app_role__analyst_label}}</option>
							<option value="2">{{translation.value.app_role__support_label}}</option>
							<option value="1">{{translation.value.app_role__viewer_label}}</option>
						</select>
					</div>
					<div class="form-group">
						<div class="form-check">
							<input class="form-check-input" type="checkbox" value="" id="activate_2fa" name="activate_2fa" dmx-bind:value="checked">
							<label class="form-check-label" for="activate_2fa">Yes, activate 2FA with Google Authenticator (or Authy)</label>
						</div>

					</div>
					<div class="form-group" dmx-hide="!activate_2fa.checked" dmx-show="activate_2fa.checked">
						<div class="form-row text-center">
							<div class="col">
								<label class="text-left"><em>This is your master key !
										<br>Scan the QR code below or type it in your app...
										<br>Then store it in a safe place as you will only see this once !</em>
								</label>
								<legend class="col-form-label" dmx-text="'YOUR MASTER KEY : ' + generate2FASecret.data.getNew2FASecretKey.data.secret_2fa"></legend>
								<img id="qr_2fa" dmx-bind:src="generate2FASecret.data.getNew2FASecretKey.data.qr_code" width="200">
								<input id="secret_2fa" name="secret_2fa" type="hidden" class="form-control" dmx-bind:value="generate2FASecret.data.getNew2FASecretKey.data.secret_2fa">
							</div>
						</div>

					</div>

					<div class="form-group">
						<button class="btn btn-success btn-lg btn-block" type="submit">{{translation.value.main_user_creation__button_create_label}} <i class="fas fa-angle-double-right fa-xs"></i></button>
					</div>
				</form>
			</div>
			<div class="d-none d-md-block col-md"></div>
		</div>
	</div>
	<script>
		$(window).resize(function(){
        // Adds a margin to the body with height = fixed top menu height
        $(document.body).css("margin-top", $("#menu-top").height());
      }).resize();
	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>