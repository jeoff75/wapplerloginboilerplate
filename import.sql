-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  mar. 14 avr. 2020 à 23:47
-- Version du serveur :  5.6.36-82.1-log
-- Version de PHP :  5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `usasandb_loginboilerplate`
--

-- --------------------------------------------------------

--
-- Structure de la table `app_config`
--

CREATE TABLE `app_config` (
  `id` int(10) UNSIGNED NOT NULL,
  `setting_name` varchar(255) NOT NULL,
  `setting_value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Application configuration';

--
-- Déchargement des données de la table `app_config`
--

INSERT INTO `app_config` (`id`, `setting_name`, `setting_value`) VALUES
(1, 'sha256_salt', 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX');

-- --------------------------------------------------------

--
-- Structure de la table `app_users`
--

CREATE TABLE `app_users` (
  `app_user_id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(256) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `two_factor_auth` varchar(256) DEFAULT NULL,
  `login_before` int(4) UNSIGNED DEFAULT NULL,
  `login_before_nonce` varchar(256) DEFAULT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `role` tinyint(3) UNSIGNED NOT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL,
  `registered` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Application users list with hashed credentials';

-- --------------------------------------------------------

--
-- Structure de la table `logging`
--

CREATE TABLE `logging` (
  `log_id` int(11) NOT NULL,
  `log_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `log_type` varchar(10) NOT NULL,
  `log_message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Log des erreurs, exceptions, debug & warning';


-- --------------------------------------------------------
--
-- Structure de la table `translations`
--

CREATE TABLE `translations` (
  `trans_id` int(11) NOT NULL,
  `text_key` varchar(255) NOT NULL,
  `lang_key` varchar(255) NOT NULL,
  `translation` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `translations`
--

INSERT INTO `translations` (`trans_id`, `text_key`, `lang_key`, `translation`) VALUES
(1, 'offer_affiliate_commission', 'fr', 'Commission (Montant ou %) (Mettre 0 si pas de commissions)'),
(2, 'offer_affiliate_commission', 'en', 'Commission (Amount or %) (Put 0 if no commissions)'),
(3, 'offer_affiliate_campaign', 'fr', 'Code campagne (ou laisser vide si campagne par défaut)'),
(4, 'offer_affiliate_campaign', 'en', 'Campaign code (or leave empty if using default campaign)'),
(5, 'offer_affiliates', 'fr', 'Affiliation'),
(6, 'offer_affiliates', 'en', 'Affiliates'),
(7, 'offer_affiliates_enabled', 'fr', 'Affiliation activée'),
(8, 'offer_affiliates_enabled', 'en', 'Affiliates activiated'),
(9, 'offer_affiliates_disabled', 'fr', 'Affiliation désactivée'),
(10, 'offer_affiliates_disabled', 'en', 'Affiliates disabled'),
(11, 'main_login__login_label', 'fr', 'Votre identifiant (adresse e-mail)'),
(12, 'main_login__login_label', 'en', 'Your login (e-mail address)'),
(13, 'main_login__password_label', 'fr', 'Votre mot de passe'),
(14, 'main_login__password_label', 'en', 'Your password'),
(15, 'main_login__remember_label', 'fr', 'Se souvenir de moi'),
(16, 'main_login__remember_label', 'en', 'Remember me'),
(17, 'main_login__button_login_label', 'fr', 'Se connecter'),
(18, 'main_login__button_login_label', 'en', 'Log in'),
(19, 'main_header__brand_label', 'fr', 'Vente.Online'),
(20, 'main_header__brand_label', 'en', 'Vente.Online'),
(21, 'main_header__home_label', 'fr', 'Accueil'),
(22, 'main_header__home_label', 'en', 'Home'),
(23, 'main_header__tools_label', 'fr', 'Outils'),
(24, 'main_header__tools_label', 'en', 'Tools'),
(25, 'main_header__pricing_label', 'fr', 'Tarifs'),
(26, 'main_header__pricing_label', 'en', 'Pricing'),
(27, 'main_header__create_account_label', 'fr', 'Créez mon compte'),
(28, 'main_header__create_account_label', 'en', 'Create my account'),
(29, 'main_header__login_label', 'fr', 'S\'identifier'),
(30, 'main_header__login_label', 'en', 'Login'),
(31, 'main_header__logout_label', 'fr', 'Déconnexion'),
(32, 'main_header__logout_label', 'en', 'Logout'),
(33, 'app_menu__test_role_superadmin', 'fr', 'Test accès superadmin'),
(34, 'app_menu__test_role_superadmin', 'en', 'Test superadmin access'),
(35, 'app_menu__test_role_admin', 'fr', 'Test accès admin'),
(36, 'app_menu__test_role_admin', 'en', 'Test admin access'),
(37, 'app_menu__test_role_viewer', 'fr', 'Test accès viewer'),
(38, 'app_menu__test_role_viewer', 'en', 'Test viewer access'),
(39, 'app_menu__test_role_developer', 'fr', 'Test accès developer'),
(40, 'app_menu__test_role_developer', 'en', 'Test developer access'),
(41, 'test_role__developer_success', 'fr', 'Félicitations, vous avez (au moins) les droits DEVELOPER !'),
(42, 'test_role__developer_success', 'en', 'Congratulations, you have (at least) DEVELOPER privileges !'),
(43, 'test_role__admin_success', 'fr', 'Félicitations, vous avez (au moins) les droits ADMIN !'),
(44, 'test_role__admin_success', 'en', 'Congratulations, you have (at least) ADMIN privileges !'),
(45, 'test_role__superadmin_success', 'fr', 'Félicitations, vous avez (au moins) les droits SUPERADMIN !'),
(46, 'test_role__superadmin_success', 'en', 'Congratulations, you have (at least) SUPERADMIN privileges !'),
(47, 'test_role__viewer_success', 'fr', 'Félicitations, vous avez (au moins) les droits VIEWER !'),
(48, 'test_role__viewer_success', 'en', 'Congratulations, you have (at least) VIEWER privileges !'),
(49, 'test_role__button_back_label', 'fr', 'Retour au panneau de contrôle'),
(50, 'test_role__button_back_label', 'en', 'Back to dashboard'),
(51, 'test_role__denied_access', 'fr', 'Votre rôle ne vous permet pas d\'accéder à cette page.'),
(52, 'test_role__denied_access', 'en', 'Access denied'),
(53, 'main_user_creation__login_label', 'en', 'Chose your username (email)'),
(54, 'main_user_creation__login_label', 'fr', 'Choisissez votre identifiant (email)'),
(55, 'main_user_creation__password_label', 'fr', 'Choisissez votre mot de passe'),
(56, 'main_user_creation__password_label', 'en', 'Chose your password'),
(57, 'main_user_creation__role_label', 'en', 'Chose your role'),
(58, 'main_user_creation__role_label', 'fr', 'Choisissez votre rôle'),
(59, 'main_user_creation__role_label', 'fr', 'Choisissez votre rôle'),
(60, 'main_user_creation__role_label', 'en', 'Chose your role'),
(61, 'app_role__superadmin_label', 'fr', 'Super-administrateur'),
(62, 'app_role__superadmin_label', 'en', 'Super-administrator'),
(63, 'app_role__admin_label', 'fr', 'Administrateur'),
(64, 'app_role__admin_label', 'en', 'Administrator'),
(65, 'app_role__developer_label', 'fr', 'Développeur'),
(66, 'app_role__developer_label', 'en', 'Developer'),
(67, 'app_role__analyst_label', 'fr', 'Analyste'),
(68, 'app_role__analyst_label', 'en', 'Analyst'),
(69, 'app_role__support_label', 'fr', 'Support'),
(70, 'app_role__support_label', 'en', 'Support'),
(71, 'app_role__viewer_label', 'fr', 'Lecture seule'),
(72, 'app_role__viewer_label', 'en', 'Viewer (read only)'),
(73, 'main_user_creation__button_create_label', 'fr', 'Créer mon compte'),
(74, 'main_user_creation__button_create_label', 'en', 'Create my account'),
(75, 'main_user_creation__account_created', 'en', 'Account created successfully. You can now log in.'),
(76, 'main_user_creation__account_created', 'fr', 'Compte crée avec succès. Vous pouvez maintenant vous connecter.'),
(77, 'main_user_creation__account_not_created', 'fr', 'Erreur à la création du compte.'),
(78, 'main_user_creation__account_not_created', 'en', 'An error was encountered during account creation.'),
(79, 'main_login__missing_password_label', 'fr', 'Merci de renseigner votre mot de passe'),
(80, 'main_login__missing_password_label', 'en', 'Please, insert your password'),
(81, 'main_login__missing_login_label', 'fr', 'Merci de renseigner votre identifiant (e-mail)'),
(82, 'main_login__missing_login_label', 'en', 'Please enter your login (e-mail)'),
(83, 'main_login__not_an_email_login_label', 'fr', 'Merci de renseigner un e-mail valide'),
(84, 'main_login__not_an_email_login_label', 'en', 'Please enter a valid e-mail'),
(85, 'main_login__email_already_exists_login_label', 'fr', 'Un compte existe déjà avec cette adresse e-mail, utilisez le lien de récupération si vous avez oublié votre mot de passe.'),
(86, 'main_login__email_already_exists_login_label', 'en', 'An account already exists with this email address, use the recovery link if you forgot your password.');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `app_config`
--
ALTER TABLE `app_config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `setting_name` (`setting_name`);

--
-- Index pour la table `app_users`
--
ALTER TABLE `app_users`
  ADD PRIMARY KEY (`app_user_id`),
  ADD UNIQUE KEY `user_login` (`email`);

--
-- Index pour la table `logging`
--
ALTER TABLE `logging`
  ADD PRIMARY KEY (`log_id`);

--
-- Index pour la table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`trans_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `app_config`
--
ALTER TABLE `app_config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `app_users`
--
ALTER TABLE `app_users`
  MODIFY `app_user_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT pour la table `logging`
--
ALTER TABLE `logging`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT pour la table `translations`
--
ALTER TABLE `translations`
  MODIFY `trans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
