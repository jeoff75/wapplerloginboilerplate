<?php
require_once(dirname(__FILE__).'/rfc6238.php');

$valid_token = false;
$secret_2fa = $_POST['secret_2fa'];  //your secret code
$token_2fa = (int) $_POST['token_2fa'];   //token to validate, for example received from device
	
if (TokenAuth6238::verify($secret_2fa,$token_2fa,1))
{
    $valid_token = true;
}
/*
$debug = TokenAuth6238::getTokenCode($secret_2fa,1);

$response = array('valid_token' => $valid_token, 'current_token' => $debug, 'passed_secret' => $secret_2fa, 'passed_token' => $token_2fa);
*/
$response = array('valid_token' => $valid_token);

header('Content-Type: application/json');
$json_pretty = json_encode($response, JSON_PRETTY_PRINT);
echo $json_pretty;

?>