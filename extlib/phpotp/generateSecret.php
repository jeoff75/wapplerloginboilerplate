<?php
require_once(dirname(__FILE__).'/rfc6238.php');

$secret_2fa = TokenAuth6238::createSecret(24);

$response = array(
    'secret_2fa' => $secret_2fa,
    'qr_code' => TokenAuth6238::getBarCodeUrl('','https://'.$_SERVER['HTTP_HOST'],$secret_2fa,'Wappler_2FA'));

header('Content-Type: application/json');
$json_pretty = json_encode($response, JSON_PRETTY_PRINT);
echo $json_pretty;
?>