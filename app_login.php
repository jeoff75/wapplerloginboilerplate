<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>Votre Ange Gardien protège vos revenus en ligne ! :)</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="css/ventes_online.css">
	<script src="dmxAppConnect/dmxAppConnect.js"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT" crossorigin="anonymous"></script>
	<script src="dmxAppConnect/dmxBootstrap4Navigation/dmxBootstrap4Navigation.js" defer=""></script>
	<script src="dmxAppConnect/dmxBootstrap4Collapse/dmxBootstrap4Collapse.js" defer=""></script>
	<style>
	</style>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.0/css/all.css" integrity="sha384-REHJTs1r2ErKBuJB0fCK99gCYsVjwxHrSU0N7I1zl9vZbggVJXRMsv/sLlOAGb4M" crossorigin="anonymous">
	<script src="dmxAppConnect/dmxFormatter/dmxFormatter.js" defer=""></script>
	<script src="dmxAppConnect/dmxBrowser/dmxBrowser.js" defer=""></script>
	<link rel="stylesheet" href="dmxAppConnect/dmxValidator/dmxValidator.css">
	<script src="dmxAppConnect/dmxValidator/dmxValidator.js" defer=""></script>
	<link rel="stylesheet" href="dmxAppConnect/dmxLightbox/dmxLightbox.css" />
	<script src="dmxAppConnect/dmxLightbox/dmxLightbox.js" defer=""></script>
	<script src="dmxAppConnect/dmxBootstrap4Modal/dmxBootstrap4Modal.js" defer=""></script>
	<script src="dmxAppConnect/dmxRouting/dmxRouting.js" defer=""></script>
</head>

<body is="dmx-app" id="app_login">
	<div class="modal" id="modal_2fa" is="dmx-bs4-modal" tabindex="-1" role="dialog" dmx-bind:show="loginForm.data.mandatory_2fa == true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header modal-no-border">
					<h5 class="modal-title" id="modal_2fa_title">Your account requires 2FA : Type in your code</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body centered">
					<form id="modal2FAForm" method="post" is="dmx-serverconnect-form" action="dmxConnect/api/security/login_2FA_step.php" dmx-on:success="!modal_2fa.modal2FAForm.data.error : browserSession.goto('app_menu.php')">
						<div class="form-group">
							<input id="token_2fa" name="token_2fa" type="number" class="form-control form-control-lg number-without-arrows centered" placeholder="Insert the 6-digits provided by your app here" required="" maxlength="6" minlength="6"
								data-rule-digits="">
							<input id="login_before_nonce" name="login_before_nonce" type="hidden" dmx-bind:value="loginForm.data.login_before_nonce">
							<small id="bs4-form-group-help1" class="form-text text-muted centered" dmx-text="modal_2fa.modal2FAForm.data.error">You have 60 seconds to type in your 6-digits code</small>
						</div>
						<button type="submit" class="btn btn-success mt-2" id="modal_2fa_submit">{{translation.value.main_login__button_login_label}} <i class="fas fa-angle-double-right fa-xs"></i></button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div is="dmx-browser" id="browserSession">

	</div>
	<dmx-serverconnect id="translate" url="dmxConnect/api/database/translate.php" dmx-param:language="browserSession.language.split('-')[0]"></dmx-serverconnect>
	<dmx-value id="translation" dmx-bind:value="translate.data.getTranslation.toKeyedObject('text_key', 'translation')"></dmx-value>
	<dmx-value id="status_2fa" dmx-bind:value="modal_2fa.modal2FAForm.data.logged" dmx-on:updated="status_2fa ? browserSession.goto('app_menu.php') : false"></dmx-value>
	<nav class="navbar navbar-expand-md navbar-light bg-light fixed-top" id="menu-top">
		<a class="navbar-brand" href="#">{{translation.value.main_header__brand_label}}</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapse1" aria-controls="collapse1" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div id="collapse1" class="collapse navbar-collapse justify-content-between">
			<ul class="navbar-nav">
				<li class="nav-item active">
					<a class="nav-link" href="./">{{translation.value.main_header__home_label}}</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">{{translation.value.main_header__tools_label}}</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">{{translation.value.main_header__pricing_label}}</a>
				</li>
				<li class="nav-item"></li>
			</ul>
			<ul class="navbar-nav">
				<li class="nav-item active mr-2">
					<a class="btn btn-success" href="#" dmx-on:click="browserSession.goto('app_create_user.php')">{{translation.value.main_header__create_account_label}} <i class="fa fa-user"></i></a>
				</li>
			</ul>
		</div>
	</nav>
	<div class="container pt-3 pt-md-5" id="cont-main-login">
		<div class="row px-lg-5 mb-2 justify-content-sm-around">
			<div class="d-none d-md-block col-md">
			</div>
			<div class="col-12 col-sm-8 col-md-6">
				<form id="loginForm" method="post" is="dmx-serverconnect-form" action="dmxConnect/api/security/login.php">
					<!-- dmx-on:success="browserSession.goto('app_menu.php')" -->
					<div class="form-group">
						<label for="username">{{translation.value.main_login__login_label}}</label>
						<input type="email" class="form-control" id="username" aria-describedby="emailHelp" placeholder="Saisissez votre e-mail pour vous connecter" name="username" value="" dmx-bind:required="true"
							dmx-bind:data-msg-required="{{translation.value.main_login__missing_login_label}}" dmx-bind:data-rule-email="true" dmx-bind:data-msg-email="{{translation.value.main_login__not_an_email_login_label}}">
					</div>
					<div class="form-group">
						<label for="password">{{translation.value.main_login__password_label}}</label>
						<input type="password" class="form-control" id="password" placeholder="Saisissez votre mot de passe" name="password" value="" dmx-bind:required="true"
							dmx-bind:data-msg-required="{{translation.value.main_login__missing_password_label}}">
					</div>
					<div class="form-group form-check">
						<input type="checkbox" class="form-check-input" id="remember" name="remember" value="1">
						<label class="form-check-label" for="remember">{{translation.value.main_login__remember_label}}</label>
					</div>
					<div class="form-group">
						<button class="btn btn-success btn-lg btn-block" type="submit">{{translation.value.main_login__button_login_label}} <i class="fas fa-angle-double-right fa-xs"></i></button>
						<input id="mandatory_2fa" name="mandatory_2fa" type="hidden" class="form-control" dmx-on:invalid="">
					</div>
				</form>
			</div>
			<div class="d-none d-md-block col-md"></div>
		</div>
	</div>
	<script>
		$(window).resize(function(){
      // Adds a margin to the body with height = fixed top menu height
      $(document.body).css("margin-top", $("#menu-top").height());
    }).resize();
	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>